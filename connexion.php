<?php include("0-config/config-genos.php"); 

	// Redirection connexion
	if(isset($_SESSION['utilisateur'])){
		header("Location:".URL_HOME);
	}

	// Verification dans la base de données
	if (!empty($_POST['login']) && !empty($_POST['mdp'])) {
		$u            = new utilisateur;
		$tab          = array();
		$tab['login'] = strtolower($_POST['login']);
		$tab['mdp']   = md5($_POST['mdp']);
		$utilisateur  = $u->Find($tab);
		
		if(!empty($utilisateur)){
			$_SESSION["utilisateur"]["id"]     = $utilisateur[0]["id"];
			$_SESSION["utilisateur"]["login"]  = $utilisateur[0]["login"];
			$_SESSION["utilisateur"]["nom"]    = $utilisateur[0]["nom"];
			$_SESSION["utilisateur"]["prenom"] = $utilisateur[0]["prenom"];
			$_SESSION["utilisateur"]["mail"]   = $utilisateur[0]["mail"];
			$_SESSION["utilisateur"]["date_crea"]   = $utilisateur[0]["date_crea"];

			header('Location:index.php');
		}

	}

?>

<!DOCTYPE html>
<html lang="fr">
	<?php head(); ?>
	<body>
		<?php menu() ?>

		<div id="connexion" class="bg-contact2" style="background-image: url('images/bg-01.jpg');">
			<div class="container-contact2">
				<div class="wrap-contact2">
					<form class="contact2-form validate-form" action="connexion.php" method="post">
						<span class="contact2-form-title">
							Connexion
						</span>

					<div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Login *" name="login" id="prenom" required="" data-validation-required-message="Veuillez saisir votre prénom.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="col-md-12">
                        	<div class="form-group">
                                <input type="password" class="form-control" placeholder="Mot de passe *" name="mdp" id="mdp" required="" data-validation-required-message="Veuillez saisir votre adresse email.">
                            	<p class="help-block text-danger"></p>
                            </div>
                        </div>

                        <div class="col-md-12">
                        	<div class="form-group">
                                <p><a href="inscription.php" >Pas encore inscrit ? Incrivez-vous dès maintenant ! </a></p>
                            </div>
                        </div>
                    </div>
                    <br><br>
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <div id="success"></div>
                            <button class="btn btn-info">Envoyer</button>
                        </div>
                    </div>

					</form>
				</div>
			</div>
		</div>

    <?php footer() ?>

	</body>
</html>
