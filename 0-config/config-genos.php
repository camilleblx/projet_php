<?php if(!session_id()) session_start();
	
	$prod = ($_SERVER["HTTP_HOST"] == "localhost") ? 0 : 1;	
	define("PROD",$prod);
	if($prod == 0) $URL_HOME 	= "http://localhost/projet_php/";
	if($prod == 1) $URL_HOME 	= "";

	define("URL_HOME",$URL_HOME);

	// Genos use auto increment field and attribute id as primary key
	// If you want to use another field/attribute as primary key set following variable
	// If it's false you'll have to add attribute primary_key in your class
	$ID_PRIMARY_DEFAULT = true;

	// Table name format
	// Windows is case unsensitive, but database on prod should be case sensitive
	// You have to choose between : lowercase, uppercase, capitalize, custom
	// If you choose personalised you'll have to add attribute table_name in your class

	$TABLE_CASE = "lowercase";

	// DATABASE
	if(PROD == 0){
		$DATABASE_NAME ='myhome';
		$DATABASE_HOST ='localhost';
		$DATABASE_PORT ='';
		$DATABASE_USER ='root';
		$DATABASE_PSWD ='';		
	}

	if(PROD == 1){
		$DATABASE_NAME ='';
		$DATABASE_HOST ='';
		$DATABASE_PORT ='';
		$DATABASE_USER ='';
		$DATABASE_PSWD ='';

		$DATABASE_NAME ='yyb47512';
		$DATABASE_HOST ='cl1-sql20';
		$DATABASE_PORT ='';
		$DATABASE_USER ='yyb47512';
		$DATABASE_PSWD ='RkFIOVo723Wh';
	}

	define("ID_PRIMARY_DEFAULT",$ID_PRIMARY_DEFAULT);
	define("TABLE_CASE",$TABLE_CASE);
	define("DATABASE_NAME",$DATABASE_NAME);
	define("DATABASE_HOST",$DATABASE_HOST);
	define("DATABASE_PORT",$DATABASE_PORT);
	define("DATABASE_USER",$DATABASE_USER);
	define("DATABASE_PSWD",$DATABASE_PSWD);

	// GENOS
	include(__DIR__."/genos.php");

	// FONCTIONS
	include(__DIR__."/code-affichage.php");

	// CLASS
	include(__DIR__."/../1-class/logement.class.php");
	include(__DIR__."/../1-class/reservation.class.php");
	include(__DIR__."/../1-class/utilisateur.class.php");