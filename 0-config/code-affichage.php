<?php

function head() {  ?>

    <head>
        <!-- META -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="projet-annuel">
        <meta name="msapplication-TileColor" content="#00aba9">
        <meta name="theme-color" content="#ffffff">
        
        <!-- TITLE -->
        <title>MyHome</title>

        <!-- BOOTSTRAP -->
        <link rel="stylesheet" type="text/css" href="<?php echo URL_HOME ?>css/bootstrap.min.css">

        <!-- BOOTSTRAP-SELECT -->
        <link rel="stylesheet" type="text/css" href="<?php echo URL_HOME ?>css/bootstrap-select.min.css">

        <!-- ANIMATE -->
        <link rel="stylesheet" type="text/css" href="<?php echo URL_HOME ?>css/animate.css">

        <!-- PRETTY-CHECKBOX -->
        <link rel="stylesheet" type="text/css" href="<?php echo URL_HOME ?>css/pretty-checkbox.min.css">

        <!-- FONTS -->
        <link rel="stylesheet" type="text/css" href="<?php echo URL_HOME ?>css/fontawesome-all.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo URL_HOME ?>css/linearicons.css">

        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="<?php echo URL_HOME ?>css/style.css">

        <link rel="stylesheet" type="text/css" href="<?php echo URL_HOME ?>css/v-calendar.min.css">

        <!-- FAVICON -->
        <link rel="apple-touch-icon" sizes="180x180"    href="<?php echo URL_HOME ?>img/logo/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo URL_HOME ?>img/logo/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo URL_HOME ?>img/logo/favicon-16x16.png">
        <link rel="manifest"                            href="<?php echo URL_HOME ?>img/logo/site.webmanifest">
        <link rel="mask-icon"                           href="/safari-pinned-tab.svg" color="#5bbad5">
    </head>
    <?php Loader() ?>
<?php }

function menu() {  ?>
        <!-- Navigation -->
        <nav id="menu" class="navbar navbar-default fixed-top intro-animated after hidden">
            <div class="container">
                <div class="navbar-header page-scroll">
                    <!-- <a class="navbar-brand page-scroll" href="#page-top"><img src="img/myhome.png" alt="Lattes theme logo">MyHome</a> -->
                    <a id="logo" class="navbar-brand page-scroll" href="index.php">MyHome</a>
                </div>
                <ul class="nav">
                  <li class="nav-item">
                    <a class="nav-link active mt-1" href="index.php">Accueil</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled mt-1" href="contact.php">Contact</a>
                </li>                
                <li class="nav-item">
                    <?php 
                    if(!empty($_SESSION['utilisateur'])){ ?>
                        <a class="nav-link disabled mt-1"  href="moncompte.php" >Mon compte</a>
                    <?php } ?>
                </li>
                <li class="nav-item btn-connexion">
                    <?php 
                    if(empty($_SESSION['utilisateur'])){ ?>
                        <a class="nav-link disabled"  href="connexion.php" >Connexion</a>
                    <?php }else{ ?>
                        <a class="nav-link disabled"  href="deconnexion.php" >Déconnexion</a>
                    <?php } ?>
                </li>
            </ul>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
<?php }




function Loader(){ ?>

    <div id="loader-container">
        <div id="loader"></div>
    </div>

<?php }

function BackTop(){ ?>

    <p id="back-top">
        <a href="#top"><i class="fa fa-angle-up"></i></a>
    </p>

<?php }

function Footer(){ ?>

    <?php BackTop() ?>
    <footer id="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 offset-md-1">
                    <a href="index.php"><img width="45" height="51" src="img/logo/logo-genos-seul.png"></a>
                    <address>
                        Powered by Genos.<br>
                        Licence sous <a id="url-license" href="https://opensource.org/licenses/MIT" target="blank">MIT license</a>.<br>
                    </address>
                    <a href="https://www.hitema.fr/"><img width="191" height="51" src="img/logo/hitema.png"></a>
                    <address>
                        <a id="url-vlis" href="https://www.hitema.fr/" target="blank">HITEMA</a> ©<br>
                        <a id="url-vlis" href="https://www.hitema.fr/" target="blank">Tous droits réservés</a> | <a id="url-vlis" href="https://www.hitema.fr/" target="blank">Mentions Légales</a>
                    </address>
                </div>
                <div id="newsletter" class="col-md-5">
                    <p>Suivez l'actualité de MyHome</p>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control col-6" placeholder="Inscrivez votre adresse mail" aria-label="Recipient's username" aria-describedby="button-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-info" type="button" id="button-addon2">OK</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php Script(); ?>
    </footer>

<?php }



function Script(){ ?>

    <!-- JQUERY -->
    <script type="text/javascript" src="<?php echo URL_HOME ?>js/jquery.min.js"></script>

    <!-- BOOTSTRAP
    Dependence : JQUERY -->
    <script type="text/javascript" src="<?php echo URL_HOME ?>js/bootstrap.bundle.min.js"></script>

    <!-- BOOTSTRAP-SELECT
    Dependence : BOOTSTRAP -->
    <script type="text/javascript" src="<?php echo URL_HOME ?>js/bootstrap-select.min.js"></script>

    <!-- VUE.JS -->
    <script type="text/javascript" src="<?php echo URL_HOME ?>js/vue.js"></script>
    <!-- <script type="text/javascript" src="<?php echo URL_HOME ?>js/vue.min.js"></script> -->
    <script type="text/javascript" src="<?php echo URL_HOME ?>js/vue-router.js"></script>

    <!-- GSAP -->
    <!-- <script type="text/javascript" src="<?php echo URL_HOME ?>js/gsap/TweenMax.min"></script>    -->

    <!-- MOMENT -->
    <script type="text/javascript" src="<?php echo URL_HOME ?>js/moment.min.js"></script>

    <!-- CHART 
    Dependence : MOMENT -->
    <script type="text/javascript" src="<?php echo URL_HOME ?>js/chart.min.js"></script>

    <!-- NOTIFY
    Dependence : JQUERY, JS -->
    <script type="text/javascript" src="<?php echo URL_HOME ?>js/notify.js"></script>

    <!-- JS -->
    <script type="text/javascript">
        URL_HOME        = "<?php echo URL_HOME; ?>";
        moment.locale('fr');
    </script>
    <script type="text/javascript" src="<?php echo URL_HOME ?>js/generale.js"></script>
    <script type="text/javascript" src="<?php echo URL_HOME ?>vendor/select2/select2.min.js"></script>
    <!-- <script type="text/javascript" src="<?php echo URL_HOME ?>js/scroller.js" async></script> -->

    <!-- FILTRES -->
    <!-- <script type="text/javascript" src="<?php echo URL_HOME ?>js/filtre/date.filtre.js""></script> -->

    <!-- COMPOSANTS -->
    <!-- <script type="text/javascript" src="<?php echo URL_HOME ?>js/composants/sondage/sondage.comp.js"></script> -->

    <!-- MAPS -->
    <script type="text/javascript" src="<?php echo URL_HOME ?>js/maps.js"></script>

    <script type="text/javascript" src="<?php echo URL_HOME ?>js/v-calendar.min.js"></script>

    


<?php }

function Map_accueil(){ ?>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDInHTnEpyec8JEe5olptD2KKWi0C822lk&libraries=places&sensor=false&callback=initAutocomplete"></script>

<?php }



function Map_description($adresse,$ville){

    $adresse = str_replace(" ", "+", $adresse);

    ?>

    <iframe
            width="100%"
            height="450"
            frameborder="0" style="border:0"
            src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDInHTnEpyec8JEe5olptD2KKWi0C822lk&q=<?php echo $adresse.",".$ville ?>" allowfullscreen>
    </iframe>

<?php }