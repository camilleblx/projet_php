<?php include("0-config/config-genos.php"); ?>

<?php
$utilisateur = new utilisateur();
$_SESSION['user'] = 'ymarivint';

if(isset($_GET['action'])){
	switch ($_GET['action']) {
		case 'ajouter':
			$utilisateur->LoadForm();
			$utilisateur->Set('mdp',md5($utlisateur->Get('mdp')));
			$utilisateur->Add();
			header("Location: index.php");
			break;			

		case 'modifier':
			if(isset($_GET['utilisateur']) && !empty($_GET['utilisateur'])){
				$utilisateur->Set("id",$_GET['utilisateur']);
				$utilisateur->Load();
				$utilisateur->LoadForm();
				$utilisateur->Update();
				header("Location: index.php");
			}
			break;		

		case 'supprimer':
			if(isset($_POST['id_utilisateur'])){
				$utilisateur->Set("id",$_POST['id_utilisateur']);
				$utilisateur->Delete();
				header("Location: index.php");
			}
			break;
	}
}
?>

<!DOCTYPE html>
<html lang="fr">
	<?php head(); ?>
	<body>
		<?php menu('index') ?>
		<main id="index">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1 id="titre">Exemple utilisation Genos & <mark v-cloak>{{ titre }}</mark> <input v-model="titre" class="form-control"></h1>
				      	<p class="lead">Genos est une librairie vous permettant d'intéragir avec vos bases de données MySql / MariaDB</p>

			      		<h3>Liste des utilisateurs</h3>
			      		<?php 
					      	$req = "SELECT * FROM utilisateur";
							$champs = array('id','login','nom','prenom','email','admin','date_crea','date_modif','auteur_modif','actif','suppr');
							$res = $utilisateur->StructList($req,$champs);
						?>
						<table class="table table-bordered table-dark">
						  <thead>
						    <tr>
						      <th scope="col">#</th>
						      <th scope="col">Login</th>
						      <th scope="col">Nom</th>
						      <th scope="col">Prenom</th>
						      <th scope="col">Email</th>
						      <th scope="col">Admin</th>
						      <th scope="col">Date_crea</th>
						      <th scope="col">Date_modif</th>
						      <th scope="col">Auteur_modif</th>
						      <th scope="col">Actif</th>
						      <th scope="col">Suppr</th>
						    </tr>
						  </thead>
						  <tbody>
						  	<?php
						  	foreach ($res as $key => $ligne) { ?>
							    <tr>
							      <!-- <th scope="row">1</th> -->
							      <th scope="row"><?php echo $ligne['id'] ?></th>
							      <td><?php echo $ligne['login'] ?></td>
							      <td><?php echo $ligne['nom'] ?></td>
							      <td><?php echo $ligne['prenom'] ?></td>
							      <td><?php echo $ligne['email'] ?></td>
							      <td><?php echo $ligne['admin'] ?></td>
							      <td><?php echo $ligne['date_crea'] ?></td>
							      <td><?php echo $ligne['date_modif'] ?></td>
							      <td><?php echo $ligne['auteur_modif'] ?></td>
							      <td><?php echo $ligne['actif'] ?></td>
							      <td><?php echo $ligne['suppr'] ?></td>
							    </tr>
						  	<?php } ?>
						  </tbody>
						</table>
						<pre class="brush:php">
					      	$req = "SELECT * FROM utilisateur";
							$champs = array('id','login','nom','prenom','email','admin','droits_non_modifiable','date_crea','date_modif','auteur_modif','actif','suppr');
							$res = $utilisateur->StructList($req,$champs);</pre>					      		

						<hr>


						<h3>Liste des utilisateurs (avec Vue.js)</h3>
						<input v-model="recherche" class="form-control">
						<table class="table table-bordered table-dark">
						  <thead>
						    <tr>
						      <th scope="col">#</th>
						      <th scope="col">Login</th>
						      <th scope="col">Nom</th>
						      <th scope="col">Prenom</th>
						      <th scope="col">Email</th>
						      <th scope="col">Admin</th>
						      <th scope="col">Date_crea</th>
						      <th scope="col">Date_modif</th>
						      <th scope="col">Auteur_modif</th>
						      <th scope="col">Actif</th>
						      <th scope="col">Suppr</th>
						    </tr>
						  </thead>
						  <tbody>
						  	<tr v-for="ligne in liste_utilisateurs">
						      <th scope="col">{{ ligne.id | recherche }}</th>
						      <th scope="col">{{ ligne.login | recherche }}</th>
						      <th scope="col">{{ ligne.nom | recherche }}</th>
						      <th scope="col">{{ ligne.prenom | recherche }}</th>
						      <th scope="col">{{ ligne.email | recherche }}</th>
						      <th scope="col">{{ ligne.admin | recherche }}</th>
						      <th scope="col">{{ ligne.date_crea | recherche }}</th>
						      <th scope="col">{{ ligne.date_modif | recherche }}</th>
						      <th scope="col">{{ ligne.auteur_modif | recherche }}</th>
						      <th scope="col">{{ ligne.actif | recherche }}</th>
						      <th scope="col">{{ ligne.suppr | recherche }}</th>
						  	</tr>
						  </tbody>
						</table>
						<pre class="brush:php">
					      	$req = "SELECT * FROM utilisateur";
							$champs = array('id','login','nom','prenom','email','admin','droits_non_modifiable','date_crea','date_modif','auteur_modif','actif','suppr');
							$res = $utilisateur->StructList($req,$champs);</pre>		      		

						<hr>

						<h3>Afficher l'utilisateur #4</h3>
			      		<?php 
							$utilisateur->Set("id",4);
							$utilisateur->Load();
						?>
						<table class="table table-bordered table-dark">
						  <thead>
						    <tr>
						      <th scope="col">#</th>
						      <th scope="col">Login</th>
						      <th scope="col">Nom</th>
						      <th scope="col">Prenom</th>
						      <th scope="col">Email</th>
						      <th scope="col">Admin</th>
						      <th scope="col">Date_crea</th>
						      <th scope="col">Date_modif</th>
						      <th scope="col">Auteur_modif</th>
						      <th scope="col">Actif</th>
						      <th scope="col">Suppr</th>
						    </tr>
						  </thead>
						  <tbody>
						    <tr>
						      <th scope="row"><?php echo $utilisateur->Get('id') ?></th>
						      <td><?php echo $utilisateur->Get('login') ?></td>
						      <td><?php echo $utilisateur->Get('nom') ?></td>
						      <td><?php echo $utilisateur->Get('prenom') ?></td>
						      <td><?php echo $utilisateur->Get('email') ?></td>
						      <td><?php echo $utilisateur->Get('admin') ?></td>
						      <td><?php echo $utilisateur->Get('date_crea') ?></td>
						      <td><?php echo $utilisateur->Get('date_modif') ?></td>
						      <td><?php echo $utilisateur->Get('auteur_modif') ?></td>
						      <td><?php echo $utilisateur->Get('actif') ?></td>
						      <td><?php echo $utilisateur->Get('suppr') ?></td>
						    </tr>
						  </tbody>
						</table>
						<pre class="brush:php">
							$utilisateur->Set("id",4);
							$utilisateur->Load();
							...
							echo $utilisateur->Get('login')</pre>

						<hr>

						<h3>Ajouter un utilisateur</h3>
						<form action="index.php?action=ajouter" method="post">
					  		<div class="form-row">
						    	<div class="form-group col-md-4">
						      		<label for="login">Login</label>
						      		<input type="text" class="form-control" id="login" name="login" placeholder="Entrez votre login">
						    	</div>									    	
						    	<div class="form-group col-md-4">
						      		<label for="mdp">Password</label>
						      		<input type="password" class="form-control" id="mdp" name="mdp" placeholder="">
						    	</div>
						    </div>
						    <div class="form-row">							    	
						    	<div class="form-group col-md-4">
						      		<label for="nom">Nom</label>
						      		<input type="text" class="form-control" id="nom" name="nom" placeholder="Entrez votre nom">
						    	</div>
							    <div class="form-group col-md-4">
							      	<label for="prenom">Prenom</label>
							      	<input type="text" class="form-control" id="prenom" name="prenom" placeholder="Entrez votre prenom">
							    </div>	
							</div>
							<div class="form-row">						    
							    <div class="form-group col-md-6">
							      	<label for="email">Email</label>
							      	<input type="email" class="form-control form-inline" id="email" name="email" placeholder="Entrez votre email">
							    </div>
							</div>
						  <button type="submit" class="btn btn-secondary">Ajouter</button>
						</form>

						<pre class="brush:php">
						$utilisateur->LoadForm();
						$utilisateur->Add();</pre>					

						<hr>

						<h3>Modifier un utilisateur</h3>
						<p> Exemple avec l'utilisateur #1</p>
						<?php
							$utilisateur->Set("id",1);
							$utilisateur->Load();
						?>
						<form action="index.php?action=modifier&utilisateur=<?php echo $utilisateur->Get('id') ?>" method="post">
					  		<div class="form-row">
						    	<div class="form-group col-md-4">
						      		<label for="login">Login</label>
						      		<input type="text" class="form-control" id="login" name="login" value="<?php echo $utilisateur->Get('login')?>" placeholder="Entrez votre login">
						    	</div>									    	
						    </div>
						    <div class="form-row">							    	
						    	<div class="form-group col-md-4">
						      		<label for="nom">Nom</label>
						      		<input type="text" class="form-control" id="nom" name="nom" value="<?php echo $utilisateur->Get('nom')?>" placeholder="Entrez votre nom">
						    	</div>
							    <div class="form-group col-md-4">
							      	<label for="prenom">Prenom</label>
							      	<input type="text" class="form-control" id="prenom" name="prenom" value="<?php echo $utilisateur->Get('prenom')?>" placeholder="Entrez votre prenom">
							    </div>	
							</div>
							<div class="form-row">						    
							    <div class="form-group col-md-6">
							      	<label for="email">Email</label>
							      	<input type="email" class="form-control" id="email" name="email" value="<?php echo $utilisateur->Get('email')?>" placeholder="Entrez votre email">
							    </div>
							</div>
						  <button type="submit" class="btn btn-secondary">Modifier</button>
						</form>
						<pre class="brush:php">
						$utilisateur->Set("id",$_GET['utilisateur']);
						$utilisateur->Load();
						$utilisateur->LoadForm();
						$utilisateur->Update();
						header("Location: index.php");</pre>							

						<hr>

						<h3>Supprimer un utilisateur</h3>
						<?php
							$req = "SELECT * FROM utilisateur";
							$res = $utilisateur->IdList($req);
						?>
						<form action="index.php?action=supprimer" method="post">
					  		<div class="form-row">
							  <div class="form-group col-md-4">
							    <label for="id_utilisateur">Example select</label>
							    <select class="form-control" id="id_utilisateur" name="id_utilisateur">
								    <?php 
								    foreach ($res as $ligne) { ?>
								    	<option value="<?php echo $ligne ?>"><?php echo $ligne ?></option>
								    <?php } ?>
							    </select>
							  </div>
						    </div>
						  <button type="submit" class="btn btn-secondary">Ajouter</button>
						</form>
						<pre class="brush:php">
						$utilisateur->Set("id",$_POST['id_utilisateur']);
						$utilisateur->Delete();</pre>							

						<hr>

						<h3>Chercher un utilisateur par le nom (toto)</h3>
						<?php
							$champs = array();
							$champs[ "nom" ] = "toto";
							$res = $utilisateur->Find($champs);
						?>
						<table class="table table-bordered table-dark">
						  <thead>
						    <tr>
						      <th scope="col">#</th>
						      <th scope="col">Login</th>
						      <th scope="col">Nom</th>
						      <th scope="col">Prenom</th>
						      <th scope="col">Email</th>
						      <th scope="col">Admin</th>
						      <th scope="col">Date_crea</th>
						      <th scope="col">Date_modif</th>
						      <th scope="col">Auteur_modif</th>
						      <th scope="col">Actif</th>
						      <th scope="col">Suppr</th>
						    </tr>
						  </thead>
						  <tbody>
						  	<?php
						  	foreach ($res as $key => $ligne) { ?>
							    <tr>
							      <!-- <th scope="row">1</th> -->
							      <th scope="row"><?php echo $ligne['id'] ?></th>
							      <td><?php echo $ligne['login'] ?></td>
							      <td><?php echo $ligne['nom'] ?></td>
							      <td><?php echo $ligne['prenom'] ?></td>
							      <td><?php echo $ligne['email'] ?></td>
							      <td><?php echo $ligne['admin'] ?></td>
							      <td><?php echo $ligne['date_crea'] ?></td>
							      <td><?php echo $ligne['date_modif'] ?></td>
							      <td><?php echo $ligne['auteur_modif'] ?></td>
							      <td><?php echo $ligne['actif'] ?></td>
							      <td><?php echo $ligne['suppr'] ?></td>
							    </tr>
						  	<?php } ?>
						  </tbody>
						</table>
						<pre class="brush:php">
						$champs = array();
						$champs[ "nom" ] = "toto";
						$res = $utilisateur->Find($champs);</pre>							

						<hr>

						<h3>Chercher un utilisateur par le nom et prénom (toto tata)</h3>
						<?php
							$champs = array();
							$champs[ "nom" ] = "toto";
							$champs[ "prenom" ] = "tata";
							$res = $utilisateur->Find($champs);
						?>
						<table class="table table-bordered table-dark">
						  <thead>
						    <tr>
						      <th scope="col">#</th>
						      <th scope="col">Login</th>
						      <th scope="col">Nom</th>
						      <th scope="col">Prenom</th>
						      <th scope="col">Email</th>
						      <th scope="col">Admin</th>
						      <th scope="col">Date_crea</th>
						      <th scope="col">Date_modif</th>
						      <th scope="col">Auteur_modif</th>
						      <th scope="col">Actif</th>
						      <th scope="col">Suppr</th>
						    </tr>
						  </thead>
						  <tbody>
						  	<?php
						  	foreach ($res as $key => $ligne) { ?>
							    <tr>
							      <!-- <th scope="row">1</th> -->
							      <th scope="row"><?php echo $ligne['id'] ?></th>
							      <td><?php echo $ligne['login'] ?></td>
							      <td><?php echo $ligne['nom'] ?></td>
							      <td><?php echo $ligne['prenom'] ?></td>
							      <td><?php echo $ligne['email'] ?></td>
							      <td><?php echo $ligne['admin'] ?></td>
							      <td><?php echo $ligne['date_crea'] ?></td>
							      <td><?php echo $ligne['date_modif'] ?></td>
							      <td><?php echo $ligne['auteur_modif'] ?></td>
							      <td><?php echo $ligne['actif'] ?></td>
							      <td><?php echo $ligne['suppr'] ?></td>
							    </tr>
						  	<?php } ?>
						  </tbody>
						</table>
						<pre class="brush:php">
						$champs = array();
						$champs[ "nom" ] = "toto";
						$champs[ "prenom" ] = "tata";
						$res = $utilisateur->Find($champs);</pre>	
					</div>
				</div>
			</div>
		</main>
		<?php footer() ?>
		<?php script() ?>
	</body>
</html>