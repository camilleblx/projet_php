<?php
include("0-config/config-genos.php");


if(isset($_GET['action']) && !empty($_GET['action'])){
	switch ($_GET['action']) {
		case 'logement':
			$l = new logement;
			$req    = "SELECT * FROM logement";
			$champs = array('id','titre','adresse','cp','position','src_image','date_crea','date_modif','auteur_modif','actif','suppr');
			$res    = $l->StructList($req,$champs);
			echo json_encode($res);
			break;

		case 'reserver':
			$r                 = new reservation;
			$r->id_utilisateur = $_SESSION['utilisateur']['id'];
			$r->id_logement    = $_POST['id_logement'];
			$r->date_debut     = $_POST['date_debut'];
			$r->date_fin       = $_POST['date_fin'];
			var_dump($r);
			$res = $r->Add();
			break;		

		case 'reservation':
			$r                           = new reservation;
			$recherche                   = array();
			$recherche['id_utilisateur'] = $_SESSION['utilisateur']['id'];
			$recherche['id_logement']    = $_POST['id_logement'];
			$res                         = $r->Find($recherche);
			echo json_encode($res);
			break;
	}
}

?>