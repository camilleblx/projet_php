<?php include("0-config/config-genos.php"); 

$id_logement = $_GET["logement"];
$l           = new logement ;
$l->id       = $id_logement;
$l->Load();

?>


<!DOCTYPE html>
<html lang="fr">
	<?php head(); ?>
	<body>
		<?php menu() ?>
		<!-- Header -->
		
		<div class="haut_description" style="background-image: url(img/logement/<?php echo $l->src_image; ?>);">
			<div class="container">
				<div class="slider-container">
					<div class="intro-text">
						<div class="intro-lead-in"> </div>
						<div class="intro-heading"> </div>
					</div>
				</div>
			</div>
		</div>
		<div id="app" class="container description">
			<div class="row">
				<div id="logement" class="col-lg-12">
					<center><h1><?php echo $l->titre; ?></h1></center>
					<div class="row">
						<div class="col-md-8">
							<h4>A propos de ce logement</h4>
							<hr>
							<div class="row">
								<div class="col-lg-3">
									Le logement
								</div>
								<div class="col-lg-9">
									<b>Adresse :    </b> <?php echo $l->adresse; ?> <br>
									<b>Ville :      </b> <?php echo $l->ville; ?> <br>
									<b>Code postal :</b> <?php echo $l->cp; ?> <br>
								</div>

							</div>
							<hr>
							<div class="row">
								<div class="col-lg-3">
									Équipements
								</div>
								<div class="col-lg-9">
									<?php echo $l->equipements; ?>
								</div>
							</div>
							<hr>
							<div class="row">	
								<div class="col-lg-3">
									Description
								</div>
								<div class="col-lg-9">
									<?php echo $l->description; ?>
								</div>
							</div>
							<br><br>
							<!--DEBUT MAP -->
								<?php Map_description($l->adresse, $l->ville) ?>
							<!--FIN MAP -->
						</div>
						<div class="col-md-4">
							<h4>Réservation</h4>
							<br>
							<v-date-picker
								show
								is-double-paned 
								mode='range'
								v-model='selectedDate'
								:input-props='{ class: "form-control", placeholder: "Date de réservation", readonly: true }'
								show-caps>
							</v-date-picker>
							<br>
							<input id="id_logement" type="hidden" value="<?php echo $_GET["logement"] ?>">
							<button class="btn btn-info" @click="Reserver()">Réserver</button>

						</div>
					</div>
				</div>
			</div>
		</div>
		                                                                                                                                     
		<p id="back-top">
			<a href="#top"><i class="fa fa-angle-up"></i></a>
		</p>
	
		<!-- ANCIEN FOOTER -->

		<!-- Modal for portfolio item 1 -->
		<div class="modal fade" id="Modal-1" tabindex="-1" role="dialog" aria-labelledby="Modal-label-1">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="Modal-label-1">Dean & Letter</h4>
					</div>
					<div class="modal-body">
						<img src="img/demo/portfolio-1.jpg" alt="img01" class="img-responsive" />
						<div class="modal-works"><span>Branding</span><span>Web Design</span></div>
						<p>Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>

	
		<!-- Modal for portfolio item 5 -->
		<div class="modal fade" id="Modal-5" tabindex="-1" role="dialog" aria-labelledby="Modal-label-5">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="Modal-label-5">Fast People</h4>
					</div>
					<div class="modal-body">
						<img src="img/demo/portfolio-5.jpg" alt="img01" class="img-responsive" />
						<div class="modal-works"><span>Branding</span><span>Web Design</span></div>
						<p>Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<?php footer() ?>
		<script src="description.app.vue.js" type="text/javascript"></script>
	</body>
</html>