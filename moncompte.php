<?php include("0-config/config-genos.php");

$u = new utilisateur;

 ?>


<!DOCTYPE html>
<html lang="fr">
	<?php head(); ?>
	<body id="page-top">
		<?php menu() ?>
		<!-- Header -->

		<div id="moncompte" class="bg-contact2" style="background-image: url('images/bg-01.jpg');">
			<div class="container-contact2">
				<div class="wrap-contact2">
					<form class="contact2-form validate-form" action="connexion.php" method="post">
						<span class="contact2-form-title">
							Mon compte
						</span>

						<div class="row">
							<div class="col-md-3">
	                            <div class="form-group">
	                                <label>Nom :</label>
	                            </div>
	                        </div>
	                        <div class="col-md-9">
	                            <div class="form-group">
	                                <input type="text" class="form-control" disabled="disabled" value="<?php echo $_SESSION["utilisateur"]["nom"]; ?>" >
	                                <p class="help-block text-danger"></p>
	                            </div>
	                        </div>
	                        <div class="col-md-3">
	                            <div class="form-group">
	                                <label>Prénom :</label>
	                            </div>
	                        </div>
	                        <div class="col-md-9">
	                        	<div class="form-group">
	                                <input type="text" class="form-control" disabled="disabled" value="<?php echo $_SESSION["utilisateur"]["prenom"]; ?>" >
	                            	<p class="help-block text-danger"></p>
	                            </div>
	                        </div>
	                        <div class="col-md-3">
	                            <div class="form-group">
	                                <label>Login :</label>
	                            </div>
	                        </div>
	                        <div class="col-md-9">
	                        	<div class="form-group">
	                                <input type="text" class="form-control" disabled="disabled" value="<?php echo $_SESSION["utilisateur"]["login"]; ?>">
	                            	<p class="help-block text-danger"></p>
	                            </div>
	                        </div>
	                        <div class="col-md-3">
	                            <div class="form-group">
	                                <label>Création :</label>
	                            </div>
	                        </div>
	                        <div class="col-md-9">
	                        	<div class="form-group">
	                                <input type="text" class="form-control" disabled="disabled" value="<?php echo $u->datedisplay($_SESSION["utilisateur"]["date_crea"], 1); ?>">
	                            	<p class="help-block text-danger"></p>
	                            </div>
	                        </div>
	                        <!--<div class="col-md-12">
	                        	<div class="form-group">
									<label for="inputEmail4">Email</label>
							     	<input type="email" class="form-control" id="inputEmail4" placeholder="Email">
							   	</div>
	                        </div>
	                    	-->
	                    </div>
					</form>
				</div>
			</div>
		</div>

		<?php Footer() ?>
		<?php Map_accueil() ?>
	</body>
</html>