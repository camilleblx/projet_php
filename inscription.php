<?php include("0-config/config-genos.php"); 

    // Verification dans la base de données
    if (!empty($_POST)) {
        $u        = new utilisateur;
        $u->LoadForm();
        $u->mdp   = md5($u->mdp);
        $u->login = strtolower(substr($u->prenom, 0, 1).$u->nom);
        $res      = $u-> Add();

        if ($res > 0) {
            header('Location:connexion.php');
        } 
    }

?>

<!DOCTYPE html>
<html lang="fr">
<?php head(); ?>
<body>
<?php menu() ?>

<div id="connexion" class="bg-contact2" style="background-image: url('images/bg-01.jpg');">
    <div class="container-contact2">
        <div class="wrap-contact2">
			<span class="contact2-form-title">Inscription</span>
            <div class="row">
                <div class="col-md-12">
                    <form id="contactForm" action="inscription.php" method="POST">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Nom *" name="nom" id="nom" required="" data-validation-required-message="Veuillez saisir votre nom.">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Prénom *" name="prenom" id="prenom" required="" data-validation-required-message="Veuillez saisir votre prénom.">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Mail *" name="mail" id="email" required="" data-validation-required-message="Veuillez saisir votre adresse email.">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="password" class="form-control" placeholder="Mot de passe *" name="mdp" id="mdp" required="" data-validation-required-message="Veuillez saisir votre adresse email.">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                        </div>
                        <br><br>
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <div id="success"></div>
                                <button class="btn btn-info">Envoyer</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>    
        </div>
    </div>
</div>


<?php footer() ?>

<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="js/main.js"></script>


</body>
</html>
