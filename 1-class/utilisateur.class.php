<?php

class utilisateur {
    use Genos ;
    public $id;
    public $login;
    public $nom;
    public $prenom;
    public $mail;
    public $mdp;
    public $date_crea;
    public $date_modif;
    public $auteur_modif;
    public $actif;
    public $suppr;
 
    public function __construct (){
        $this ->id           = 0;
        $this ->login        = "";
        $this ->nom          = "";
        $this ->prenom       = "";
        $this ->mail         = "";
        $this ->mdp          = "";
        $this ->date_crea    = date('Y-m-d');
        $this ->date_modif   = "0000-00-00";
        $this ->auteur_crea  = "";
        $this ->auteur_modif = "";
        $this ->actif        = 1;
        $this ->suppr        = 0;
    } 
}

?>