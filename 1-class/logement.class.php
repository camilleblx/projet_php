<?php

class logement {
    use Genos ;
    public $id;
    public $titre;
    public $adresse;
    public $ville;
    public $cp;
    public $tel;
    public $type;
    public $chambres;
    public $position;
    public $src_image;
    public $date_crea;
    public $date_modif;
    public $auteur_modif;
    public $actif;
    public $suppr;
    public $equipements;
    public $description;
 
    public function __construct (){
        $this ->id           = 0;
        $this ->titre        = "";
        $this ->adresse      = "";
        $this ->public       = "";
        $this ->cp           = "";
        $this ->tel          = "";
        $this ->type         = "";
        $this ->chambres     = "";
        $this ->position     = "";
        $this ->src_image    = "";
        $this ->date_crea    = date('Y-m-d');
        $this ->date_modif   = "0000-00-00";
        $this ->auteur_crea  = "";
        $this ->auteur_modif = "";
        $this ->actif        = 1;
        $this ->suppr        = 0;
        $this ->equipements  = "";
        $this ->description  = "";
        } 
}

?>