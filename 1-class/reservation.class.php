<?php

class reservation {
    use Genos ;
    public $id;
    public $id_logement;
    public $id_utilisateur;
    public $date_debut;
    public $date_fin;
    public $date_crea;
    public $date_modif;
    public $auteur_crea;
    public $auteur_modif;
    public $actif;
    public $suppr;
 
    public function __construct (){
        $this ->id             = 0;
        $this ->id_logement    = 0;
        $this ->id_utilisateur = 0;
        $this ->date_debut     = "0000-00-00";
        $this ->date_fin       = "0000-00-00";
        $this ->date_crea      = date('Y-m-d');
        $this ->date_modif     = "0000-00-00";
        $this ->auteur_crea    = "";
        $this ->auteur_modif   = "";
        $this ->actif          = 1;
        $this ->suppr          = 0;
    } 
}

?>