<?php include("0-config/config-genos.php"); ?>

<!DOCTYPE html>
<html lang="fr">
<?php Head(); ?>
<body>
	<?php Menu('index') ?>
	<main>
		<?php 
			if(isset($_SESSION['utilisateur'])) {
				unset($_SESSION['utilisateur']);
				header('Location:connexion.php');
			}
		?>			
	</main>
</body>


