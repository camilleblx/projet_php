new Vue({
	el: '#app',
	data: {
		id_logement: 0,
		date_reservation:"",
		mode: 'single',
  		selectedDate: {
    		start: new Date(),
    		end: new Date(new Date().getTime()+(7*24*60*60*1000))
  		}
	},
	mounted:function(){
		this.id_logement = $("#id_logement").val();
		this.Reservation();
	},
	methods:{
		Reservation:function(){
			var scope         = this;
			var datas         = {};
			datas.id_logement = scope.id_logement;
			$.ajax({
			    url:"data.php?action=reservation",
			    type:"POST",
			    data:datas,
			    success:function(res){
					var reservation          = JSON.parse(res);
					var date_debut           = new Date(reservation[0]['date_debut']);
					var date_fin             = new Date(reservation[0]['date_fin']);
					scope.selectedDate.start = date_debut;
					scope.selectedDate.end   = date_fin;
			    },
			    error:function(){
					Notify("danger","Erreur Ajax");
			    }
			});
		},
		Reserver:function(){
			var scope = this;

			var annee_debut = scope.selectedDate.start.getFullYear();
			var mois_debut  = (scope.selectedDate.start.getMonth() < 10 ? "0"+scope.selectedDate.start.getMonth() : scope.selectedDate.start.getMonth());
			var jours_debut = (scope.selectedDate.start.getDate() < 10 ? "0"+scope.selectedDate.start.getDate() : scope.selectedDate.start.getDate());			

			var annee_fin = scope.selectedDate.end.getFullYear();
			var mois_fin  = (scope.selectedDate.end.getMonth() < 10 ? "0"+scope.selectedDate.end.getMonth() : scope.selectedDate.end.getMonth());	
			var jours_fin = (scope.selectedDate.end.getDate() < 10 ? "0"+scope.selectedDate.end.getDate() : scope.selectedDate.end.getDate());	

			var datas         = {};
			datas.id_logement = scope.id_logement;
			datas.date_debut  = annee_debut+"-"+mois_debut+"-"+jours_debut;
			datas.date_fin    = annee_fin+"-"+mois_fin+"-"+jours_fin;

			$.ajax({
			    url:"data.php?action=reserver",
			    type:"POST",
			    data:datas,
			    success:function(res){
					// scope.date_reservation = JSON.parse(res);
					Notify("success","La réservation a été enregistré");
			    },
			    error:function(){
					Notify("danger","Erreur Ajax");
			    }
			});
		},
	}
})