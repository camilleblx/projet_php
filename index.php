<?php include("0-config/config-genos.php"); 

$l               = new logement();
$req             = "SELECT * FROM logement WHERE suppr = 0";
$champs          = $l->FieldList();
$liste_logements = $l->StructList($req,$champs);

// var_dump($_SESSION);

// echo $_SESSION['utilisateur']["id"];

?>


<!DOCTYPE html>
<html lang="fr">
	<?php head(); ?>
	<body id="page-top">
		<?php menu() ?>
		<!-- Header -->
		<header>
			<div class="container">
				<div class="slider-container">
					<div class="intro-text intro-animated hidden">
						<div class="intro-lead-in">Votre maison, on la voit comme vous</div>
						<div class="intro-heading">Explorer MyHome</div>
					</div>
				</div>
			</div>
		</header>

		<section id="maps">
			<div class="row no-gutters justify-content-md-center maps-animated">
				<div class="col-md-12 text-center">
					<h2 class="titre">…quelques-uns de nos biens</h2>
					<input id="maps-rechercher" class="form-control col-md-6 offset-md-3 controls" type="text" placeholder="Rechercher un bien immobilier" aria-describedby="btn-maps-rechercher">
				</div>
				<div class="col-md-12">
					<!--DEBUT MAP-->
					<div id="map"></div> 
					<!--FIN MAP-->
				</div>
			</div>
		</section>

		<section id="logements">
			<div class="container-fluid">
			   <div class="col-md-12">
			        <div class="row">
						<?php 
						foreach ($liste_logements as $key => $ligne) { ?>
							<article class="col-md-4 col-sm-6">
								<div class="col">
									<div class="bien">
										<a href="description.php?logement=<?php echo $ligne['id'] ?>" target="_blank">
											<img src="img/logement/<?php echo $ligne['src_image'] ?>" class="img-responsive img-logement">
					              		</a>  
										<span class="bandeau"><p>LOCATION</p></span>
									</div>
								</div><!-- Fin de col md 12 -->

								<div class="col text-center">
									<h3><?php echo $ligne['titre'] ?></h3>
			               		</div> <!-- Fin de col md 12 -->
		              		</article>
						<?php } ?>

			   		</div>  <!-- Fin col-md-12 -->
				</div> <!-- row -->
			</div> <!-- Fin container-fluid -->
		</section>
		
		<?php Footer() ?>
		<?php Map_accueil() ?>
	</body>
</html>